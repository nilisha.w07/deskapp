package gnukhata.views;

import java.awt.event.KeyAdapter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.text.TableView.TableRow;

import gnukhata.globals;
import gnukhata.controllers.reportController;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ControlEditor;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.custom.TableCursor;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.jopendocument.dom.ODPackage;
import org.jopendocument.dom.OOUtils;
import org.jopendocument.dom.spreadsheet.Sheet;
import org.jopendocument.dom.spreadsheet.SpreadSheet;

public class viewTrialBalReport extends Composite {
	int counter = 0;
	static Display display;
	ODPackage sheetStream;
	Table tblnettrialbal;
	TableItem headerRow;
	TableColumn srno;
	TableColumn accname;
	TableColumn grpname;
	TableColumn dr;
	TableColumn cr;
	Label lblsrno;
	Label lblaccname;
	Label lblgrpname;
	Label lbldr;
	Label lblcr;
	Button btnViewTbForAccount;
	Button btnPrint;
    String strdate;
	NumberFormat nf;
	Vector<Object> printNetTrialBalance = new Vector<Object>();
	ArrayList<Button> accounts = new ArrayList<Button>();
	String endDateParam = ""; 
	public viewTrialBalReport(Composite parent,String endDate, int style, Object[]tbData)
	{
		super(parent,style);
		
		FormLayout formlayout = new FormLayout();
		FormData layout=new FormData();
		this.setLayout(formlayout);
		
		Label lblLogo = new Label(this, SWT.None);
		layout = new FormData();
		layout.top = new FormAttachment(1);
		layout.left = new FormAttachment(63);
		layout.right = new FormAttachment(87);
		layout.bottom = new FormAttachment(9);
		//layout.right = new FormAttachment(95);
		//layout.bottom = new FormAttachment(18);
		//lblLogo.setSize(getClientArea().width, getClientArea().height);
		lblLogo.setLocation(getClientArea().width, getClientArea().height);
		lblLogo.setLayoutData(layout);
		//Image img = new Image(display,"finallogo1.png");
		lblLogo.setImage(globals.logo);
		
		Label lblOrgDetails = new Label(this,SWT.NONE);
		lblOrgDetails.setFont( new Font(display,"Times New Roman", 11, SWT.BOLD ) );
		lblOrgDetails.setText(globals.session[1]+"\n"+"For Financial Year "+"From "+globals.session[2]+" To "+globals.session[3] );
		layout = new FormData();
		layout.top = new FormAttachment(2);
		layout.left = new FormAttachment(2);
		//layout.right = new FormAttachment(53);
		//layout.bottom = new FormAttachment(18);
		lblOrgDetails.setLayoutData(layout);

		/*Label lblLink = new Label(this,SWT.None);
		lblLink.setText("www.gnukhata.org");
		lblLink.setFont(new Font(display, "Times New Roman", 11, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment(lblLogo,0);
		layout.left = new FormAttachment(65);
		//layout.right = new FormAttachment(33);
		//layout.bottom = new FormAttachment(19);
		lblLink.setLayoutData(layout);*/
		 
		Label lblLine = new Label(this,SWT.NONE);
		lblLine.setText("-------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		lblLine.setFont(new Font(display, "Times New Roman",18, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment( lblLogo , 2);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(99);
		layout.bottom = new FormAttachment(22);
		lblLine.setLayoutData(layout);
	
		
		lblOrgDetails = new Label(this,SWT.NONE);
		lblOrgDetails.setFont( new Font(display,"Times New Roman", 12, SWT.ITALIC| SWT.BOLD ) );
		strdate=endDate.substring(8)+"-"+endDate.substring(5,7)+"-"+endDate.substring(0, 4);
		endDateParam = endDate;
		lblOrgDetails.setText("Net Trial Balance For The Period From "+globals.session[2]+" To "+strdate);
		layout = new FormData();
		layout.top = new FormAttachment(22);
		layout.left = new FormAttachment(25);
		lblOrgDetails.setLayoutData(layout);
		
		
		tblnettrialbal = new Table (this, SWT.MULTI | SWT.BORDER | SWT.FULL_SELECTION|SWT.LINE_SOLID);
		tblnettrialbal.setLinesVisible (true);
		tblnettrialbal.setHeaderVisible (false);
		layout = new FormData();
		layout.top = new FormAttachment(lblOrgDetails,10);
		layout.left = new FormAttachment(10);
		layout.right = new FormAttachment(90);
		layout.bottom = new FormAttachment(91);
		tblnettrialbal.setLayoutData(layout);
		btnViewTbForAccount =new Button(this,SWT.PUSH);
		btnViewTbForAccount.setText("&Back To Trial Balance");
		btnViewTbForAccount.setFont(new Font(display,"Times New Roman",10,SWT.BOLD));
		layout = new FormData();
		layout.top=new FormAttachment(tblnettrialbal,20);
		layout.left=new FormAttachment(35);
		btnViewTbForAccount.setLayoutData(layout);
		
		btnPrint =new Button(this,SWT.PUSH);
		btnPrint.setText(" &Print ");
		btnPrint.setFont(new Font(display,"Times New Roman",10,SWT.BOLD));
		layout = new FormData();
		layout.top=new FormAttachment(tblnettrialbal,20);
		layout.left=new FormAttachment(60);
		btnPrint.setLayoutData(layout);

		
		double dr = 0.00;
		this.makeaccessible(tblnettrialbal);
		this.getAccessible();
		//this.setEvents();
		//this.pack();
		this.setBounds(this.getDisplay().getPrimaryMonitor().getBounds());
		int shellwidth = this.getClientArea().width;
		/*MessageBox m = new MessageBox(new Shell(),SWT.OK);
		m.setMessage(Integer.toString(shellwidth));
		m.open();*/
		try {
			sheetStream = ODPackage.createFromStream(this.getClass().getResourceAsStream("/templates/NetTrialBal.ots"),"NetTrialBal");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		this.setReport(tbData);
		setEvents();
		
	}
	
	private void setReport(Object[] tbData)
	{
		String[] columns;
		String[] orgname;
		orgname=new String[]{ globals.session[1].toString()};
		columns=new String[]{"Sr.No","Account Name","Group Name","Debit","Credit"};
		
		lblsrno = new Label(tblnettrialbal,SWT.BORDER|SWT.CENTER);
		lblsrno.setFont(new Font(display, "Times New Roman", 10, SWT.BOLD));
		lblsrno.setText("Sr. No.");
		lblaccname = new Label(tblnettrialbal,SWT.BORDER|SWT.CENTER);
		lblaccname.setFont(new Font(display, "Times New Roman", 10, SWT.BOLD));
		lblaccname.setText("    Account Name    ");
		lblgrpname = new Label(tblnettrialbal,SWT.BORDER|SWT.CENTER);
		lblgrpname.setFont(new Font(display, "Times New Roman", 10, SWT.BOLD));
		lblgrpname.setText("  Group Name  ");
		
		lbldr = new Label(tblnettrialbal,SWT.BORDER|SWT.CENTER);
		lbldr.setFont(new Font(display, "Times New Roman", 10, SWT.BOLD));
		lbldr.setText("    	 Debit     	 ");
		
		lblcr = new Label(tblnettrialbal,SWT.BORDER|SWT.CENTER);
		lblcr.setFont(new Font(display, "Times New Roman", 10, SWT.BOLD));
		lblcr.setText("    	Credit     	 ");
		
					
		headerRow = new TableItem(tblnettrialbal, SWT.BORDER|SWT.COLOR_DARK_RED);
		
		TableItem[] items = tblnettrialbal.getItems();
		srno = new TableColumn(tblnettrialbal,SWT.CENTER);
		accname = new TableColumn(tblnettrialbal,SWT.LEFT);
		grpname = new TableColumn(tblnettrialbal,SWT.CENTER);
		dr = new TableColumn(tblnettrialbal, SWT.RIGHT);
		cr = new TableColumn(tblnettrialbal, SWT.RIGHT);
		
		TableEditor editorsrno = new TableEditor(tblnettrialbal);
		editorsrno.grabHorizontal = true;
		editorsrno.setEditor(lblsrno,items[0],0);
		

		
		TableEditor editorAccName = new TableEditor(tblnettrialbal);
		editorAccName.grabHorizontal = true;
		editorAccName.setEditor(lblaccname,items[0],1);
		
		TableEditor editorgrpName = new TableEditor(tblnettrialbal);
		editorgrpName.grabHorizontal = true;
		editorgrpName.setEditor(lblgrpname,items[0],2);
		
		TableEditor editorDr = new TableEditor(tblnettrialbal);
		editorDr.grabHorizontal = true;
		editorDr.setEditor(lbldr,items[0],3);
		
		TableEditor editorCr = new TableEditor(tblnettrialbal);
		editorCr.grabHorizontal = true;
		editorCr.setEditor(lblcr,items[0],4);
		final int tblwidth = tblnettrialbal.getClientArea().width;
		//cr.setResizable(false);
		tblnettrialbal.addListener(SWT.MeasureItem, new Listener() 
		{
			@Override
			public void handleEvent(Event event) {
				// TODO Auto-generated method stub
				System.out.println("event fired");
				srno.setWidth(5 * tblwidth / 100);
				accname.setWidth(32 * tblwidth / 100);
				grpname.setWidth(25* tblwidth / 100);
				dr.setWidth(15 * tblwidth / 100);
				cr.setWidth(15 * tblwidth / 100);
				event.height = 32;
			}
		});

		srno.pack();
		accname.pack();
		grpname.pack();
		dr.pack();
		cr.pack();
		
		btnPrint.setData("printorgname",orgname);
		btnPrint.setData("printcolumns",columns);
		
		int totalColWidth = srno.getWidth() + accname.getWidth() + grpname.getWidth() + dr.getWidth() + cr.getWidth();
				//MessageBox msgcols = new MessageBox(new Shell(),SWT.OK);
				//msgcols.setMessage(Integer.toString(totalColWidth));
				//msgcols.open();

				//tblnettrialbal.pack();		
		double dr = 0.00;
		double cr = 0.00;
		
		for(int tbcounter = 0; tbcounter< tbData.length; tbcounter ++)
		{
			TableItem tbrow = new TableItem(tblnettrialbal , SWT.NONE);
			tbrow.setFont(new Font(display, "Times New Roman", 10, SWT.NORMAL));
			Object[] trialRecord = (Object[]) tbData[tbcounter];
			Object[] tbrecord = (Object[]) tbData[tbcounter];
			

			if(tbcounter < tbData.length-1)
			{
			tbrow.setText(0, tbrecord[0].toString());
			
			Button btnAccount = new Button(tblnettrialbal , SWT.FLAT);
			btnAccount.setFont(new Font(display, "Times New Roman", 10, SWT.BOLD));
			TableEditor btnedit = new TableEditor(tblnettrialbal);
			btnAccount.setText(tbrecord[1].toString());
			btnedit.grabHorizontal = true;
			btnedit.setEditor(btnAccount,tbrow,1 );
			accounts.add(btnAccount);
			//tbrow.setText(1, tbrecord[1].toString());
			tbrow.setText(2, tbrecord[2].toString());
			tbrow.setText(3, tbrecord[3].toString());
			tbrow.setText(4, tbrecord[4].toString());
		
			Object[] printableRow = new Object[]{trialRecord[0], trialRecord[1],trialRecord[2],trialRecord[3], trialRecord[4]};
			printNetTrialBalance.add(printableRow);
			}
			else
			{
				
				tbrow.setText(0,"");
				tbrow.setText(1,"");
				tbrow.setText(2,"Total");
				tbrow.setFont(new Font(display, "Times New Roman",10, SWT.BOLD));
				tbrow.setText(3, tbrecord[0].toString());
				tbrow.setText(4, tbrecord[1].toString());
				Object[] printableRow = new Object[]{"","","Total", trialRecord[0],trialRecord[1]};
				printNetTrialBalance.add(printableRow);
				/*tbrow.setText(3, trialRecord[0].toString());
				tbrow.setText(4, trialRecord[1].toString());*/
				
			}
			
		}
		
			tblnettrialbal.getVerticalBar().setVisible(true);
			nf = NumberFormat.getInstance();
			nf.setGroupingUsed(false);
			nf.setMaximumFractionDigits(2);
			nf.setMinimumFractionDigits(2);
			
			TableItem closingRow = new TableItem(tblnettrialbal , SWT.NONE|SWT.SEPARATOR);
			closingRow.setFont(new Font(display, "Times New Roman", 10, SWT.NORMAL));
			
			//now get the Total Dr and Total Cr.
			//both can be got from the last row of tbdata.
			//get the last row (tbdata.len -1)
			//then access row[0] for TotalDr and row[1] for totalCr.
			//if dr is greater than cr then it is a dr balance.
			//substract the cr amount from dr to get the diff.
			//do exactly the other way round for cr > dr.
			Object[] lastRow = (Object[]) tbData[tbData.length-1 ];
			

			dr= Double.parseDouble(lastRow[0].toString());
			cr= Double.parseDouble(lastRow[1].toString());
			Double diffbal = 0.00;
			
			
			if(dr > cr)
			{
				diffbal = dr - cr;
				closingRow.setText(1,"    Difference in Trial Balance");
				closingRow.setFont(new Font(display, "Times New Roman",10, SWT.BOLD|SWT.CENTER));
				closingRow.setText(4,nf.format(diffbal));
				Object[] printableRow = new Object[]{""," Difference in Trial Balance","","", diffbal};
				printNetTrialBalance.add(printableRow);
				
			}
			if(cr > dr)
			{
				diffbal = cr - dr;
				closingRow.setText(1,"    Difference in Trial Balance");
				closingRow.setFont(new Font(display, "Times New Roman",10, SWT.BOLD|SWT.CENTER));
				closingRow.setText(3,nf.format(diffbal));
				Object[] printableRow = new Object[]{""," Difference in Trial Balance","", diffbal,""};
				printNetTrialBalance.add(printableRow);				
				
			}
			
			TableItem totaldrcr = new TableItem(tblnettrialbal , SWT.NONE|SWT.SEPARATOR);
			
			totaldrcr.setFont(new Font(display, "Times New Roman",10, SWT.BOLD));
			if(dr>cr)
			{
				totaldrcr.setText(3,lastRow[0].toString());
				totaldrcr.setText(4,lastRow[0].toString());
				Object[] printableRow = new Object[]{"","","",lastRow[0],lastRow[0]};
				printNetTrialBalance.add(printableRow);
			}
			if(cr>dr)
			{
				totaldrcr.setText(3,lastRow[1].toString());
				totaldrcr.setText(4,lastRow[1].toString());
				Object[] printableRow = new Object[]{"","","",lastRow[1],lastRow[1]};
				printNetTrialBalance.add(printableRow);
			}
		tblnettrialbal.pack();
					

		

	}
	private void setEvents()
	{
		
		btnViewTbForAccount.addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.ARROW_RIGHT)
				{
					btnPrint.setFocus();
				}
				
			}
		});
		
		
		btnPrint.addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.ARROW_LEFT)
				{
					btnViewTbForAccount.setFocus();
				}
				
			}
		});
		
		btnViewTbForAccount.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				
				
				Composite grandParent = (Composite) btnViewTbForAccount.getParent().getParent();
				btnViewTbForAccount.getParent().dispose();
					
					viewTrialBalance vl=new viewTrialBalance(grandParent,SWT.NONE);
					vl.setSize(grandParent.getClientArea().width,grandParent.getClientArea().height);
				}
		
		});
				tblnettrialbal.setFocus();
		tblnettrialbal.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusGained(arg0);
				if(accounts.isEmpty())
				{
					btnViewTbForAccount.setFocus();
				}
				else
				{
				accounts.get(0).setFocus();
				}
				
			}
		} );
		for(int accountcounter = 0; accountcounter < accounts.size(); accountcounter ++ )
		{
			accounts.get(accountcounter).addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent arg0) {
					// TODO Auto-generated method stub
					//super.widgetSelected(arg0);
					Button btncurrent = (Button) arg0.widget;
					String fromdate=globals.session[2].toString().substring(6)+"-"+globals.session[2].toString().substring(3,5)+"-"+globals.session[2].toString().substring(0,2);
					

					Composite grandParent = (Composite) btncurrent.getParent().getParent().getParent();
					String accName = btncurrent.getText();
					MessageBox msg = new MessageBox(new Shell(),SWT.OK);
					msg.setMessage(endDateParam);
					//msg.open();
					reportController.showLedger(grandParent, accName,fromdate,endDateParam, "No Project", true, true, false,"Net Trial Balance","" );
					btncurrent.getParent().getParent().dispose();

					
				}
			});
			accounts.get(accountcounter).addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if(arg0.keyCode==SWT.ARROW_DOWN && counter < accounts.size()-1 )
					{
						counter++;
						if(counter >= 0 && counter < accounts.size())
						{
							
						accounts.get(counter).setFocus();
						}
					}
					if(arg0.keyCode==SWT.ARROW_UP && counter > 0)
					{
						counter--;
						if(counter >= 0&& counter < accounts.size())
						{
							
							accounts.get(counter).setFocus();
						}
					}
				
					
				}
			});
			

			}
		
		btnPrint.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent arg0) {
					// TODO Auto-generated method stub
					//super.widgetSelected(arg0);
					String[] strPrintCol = new String[]{"","","","",""};
					Object[][] finaldata=new Object[printNetTrialBalance.size() +1][strPrintCol.length];
					Object[] firstRow = new Object[]{"Sr.No.", "Account Name","Group Name","Debit","Credit" };
					finaldata[0] = firstRow;
					
					for(int counter=0; counter < printNetTrialBalance.size(); counter++)
					{
						Object[] printrow=(Object[])printNetTrialBalance.get(counter);
				//		orgdata[counter]=printrow;
						finaldata[counter +1]=printrow;
					}	
					//printLedgerData.copyInto(finalData);
					
					TableModel model = new DefaultTableModel(finaldata,strPrintCol);
					try 
					{
						final File NetTrialBalReport = new File("/tmp/gnukhata/Report_Output/NetTrialBalance");
						final Sheet NetTrialBalReportSheet = sheetStream.getSpreadSheet().getFirstSheet();
						NetTrialBalReportSheet.ensureRowCount(100000);
						NetTrialBalReportSheet.getCellAt(0, 0).setValue(globals.session[1].toString());
						NetTrialBalReportSheet.getCellAt(0, 1).setValue("Net Trial Balance For The Period From "+globals.session[2]+" To "+strdate);
						for(int rowcounter = 0; rowcounter < printNetTrialBalance.size(); rowcounter ++ )
						{
							Object[] printRow = (Object[]) printNetTrialBalance.get(rowcounter);
							NetTrialBalReportSheet.getCellAt(0,rowcounter +3).setValue(printRow[0].toString());
							NetTrialBalReportSheet.getCellAt(1,rowcounter +3).setValue(printRow[1].toString());
							NetTrialBalReportSheet.getCellAt(2,rowcounter +3).setValue(printRow[2].toString());
							NetTrialBalReportSheet.getCellAt(3,rowcounter +3).setValue(printRow[3].toString());
							NetTrialBalReportSheet.getCellAt(4,rowcounter +3).setValue(printRow[4].toString());
						}
						OOUtils.open(NetTrialBalReportSheet.getSpreadSheet().saveAs(NetTrialBalReport));
						//OOUtils.open(AccountReport);
					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});				
		
		}
		

	
	
	
	public void makeaccessible(Control c)
	{
	/*
	 * getAccessible() method is the method of class Controlwhich is the
	 * parent class of all the UI components of SWT including Shell.so when
	 * the shell is made accessible all the controls which are contained by
	 * that shell are made accessible automatically.
	 */
		c.getAccessible();
	}



	protected void checkSubclass()
	{
	//this is blank method so will disable the check that prevents subclassing of shells.
	}
	/*public static void main(String[] args)
	{
		Display d = new Display();
		Shell s= new Shell(d);
		/*int vouchercode = 0;
		String voucherType = null;
		viewTrialBalReport vtbr=new viewTrialBalReport(s, SWT.NONE);
		vtbr.setSize(s.getClientArea().width, s.getClientArea().height );
		
		//s.setSize(400, 400);
		s.pack();
		s.open();
		while (!s.isDisposed() ) {
			if (!d.readAndDispatch())
			{
				 d.sleep();
				 if(! s.getMaximized())
				 {
					 s.setMaximized(true);
				 }
			}
		}
		
	}*/
}
