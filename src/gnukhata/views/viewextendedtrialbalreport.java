package gnukhata.views;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import gnukhata.globals;
import gnukhata.controllers.reportController;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.jopendocument.dom.ODPackage;
import org.jopendocument.dom.OOUtils;
import org.jopendocument.dom.spreadsheet.Sheet;
import org.jopendocument.dom.spreadsheet.SpreadSheet;

public class viewextendedtrialbalreport extends Composite{
	static Display display;
	ODPackage sheetStream;
	int counter=0;
	Table tblextendedtrialbal;
	TableItem headerRow;
	TableColumn srno;
	TableColumn accname;
	TableColumn grpname;
	TableColumn openingbal;
	TableColumn totaldrtransac;
	TableColumn totalcrtransac;
	TableColumn drbal;
	TableColumn crbal;
	Label lblsrno;
	Label lblaccname;
	Label lblgrpname;
	Label lblopeningbal;
	Label lbltotaldrtransac;
	Label lbltotalcrtransac;
	Label lbldrbal;
	Label lblcrbal;
	Button btnViewTbForAccount;
	Button btnPrint;

	NumberFormat nf;
	Vector<Object> printExtendedTrial = new Vector<Object>();
	ArrayList<Button> accounts = new ArrayList<Button>();
	String endDateParam = "";
	public viewextendedtrialbalreport(Composite parent, String endDate, int style, Object[]tbData2 )
	{
		super(parent,style);
		endDateParam = endDate;
		FormLayout formlayout = new FormLayout();
		FormData layout=new FormData();
		this.setLayout(formlayout);
		String strdate;
		strdate=endDate.substring(8)+"-"+endDate.substring(5,7)+"-"+endDate.substring(0, 4);
		Label lblLogo = new Label(this, SWT.None);
		layout = new FormData();
		layout.top = new FormAttachment(1);
		layout.left = new FormAttachment(63);
		layout.right = new FormAttachment(87);
		layout.bottom = new FormAttachment(9);
		//layout.right = new FormAttachment(95);
		//layout.bottom = new FormAttachment(18);
		//lblLogo.setSize(getClientArea().width, getClientArea().height);
		lblLogo.setLocation(getClientArea().width, getClientArea().height);
		lblLogo.setLayoutData(layout);
		//Image img = new Image(display,"finallogo1.png");
		lblLogo.setImage(globals.logo);
		
		Label lblOrgDetails = new Label(this,SWT.NONE);
		lblOrgDetails.setFont( new Font(display,"Times New Roman", 11, SWT.BOLD ) );
		lblOrgDetails.setText(globals.session[1]+"\n"+"For Financial Year "+"From "+globals.session[2]+" To "+globals.session[3] );
		layout = new FormData();
		layout.top = new FormAttachment(2);
		layout.left = new FormAttachment(2);
		//layout.right = new FormAttachment(53);
		//layout.bottom = new FormAttachment(18);
		lblOrgDetails.setLayoutData(layout);

		/*Label lblLink = new Label(this,SWT.None);
		lblLink.setText("www.gnukhata.org");
		lblLink.setFont(new Font(display, "Times New Roman", 11, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment(lblLogo,0);
		layout.left = new FormAttachment(65);
		//layout.right = new FormAttachment(33);
		//layout.bottom = new FormAttachment(19);
		lblLink.setLayoutData(layout);*/
		 
		Label lblLine = new Label(this,SWT.NONE);
		lblLine.setText("-------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		lblLine.setFont(new Font(display, "Times New Roman",18, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment( lblLogo , 2);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(99);
		layout.bottom = new FormAttachment(22);
		lblLine.setLayoutData(layout);
		

		

		

		lblOrgDetails = new Label(this,SWT.NONE);
		lblOrgDetails.setFont( new Font(display,"Times New Roman", 12, SWT.ITALIC| SWT.BOLD ) );
		strdate=endDate.substring(8)+"-"+endDate.substring(5,7)+"-"+endDate.substring(0, 4);
		lblOrgDetails.setText("Extended Trial Balance For The Period From "+globals.session[2]+" To "+strdate);
		layout = new FormData();
		layout.top = new FormAttachment(22);
		layout.left = new FormAttachment(25);
		//layout.right = new FormAttachment(53);
		//layout.bottom = new FormAttachment(18);
		lblOrgDetails.setLayoutData(layout);
		
		//tblextendedtrialbal = new Table (this, SWT.MULTI | SWT.BORDER | SWT.FULL_SELECTION);
		tblextendedtrialbal=new Table(this, SWT.MULTI|SWT.BORDER|SWT.LINE_SOLID|SWT.FULL_SELECTION);
		tblextendedtrialbal.setLinesVisible (true);
		tblextendedtrialbal.setHeaderVisible (false);
		layout = new FormData();
		layout.top = new FormAttachment(lblOrgDetails,10);
		layout.left = new FormAttachment(7);
		layout.right = new FormAttachment(85);
		layout.bottom = new FormAttachment(91);
		tblextendedtrialbal.setLayoutData(layout);
		
		
		
		  btnViewTbForAccount =new Button(this,SWT.PUSH);
			btnViewTbForAccount.setText("&Back To Trial Balance");
			btnViewTbForAccount.setFont(new Font(display,"Times New Roman",10,SWT.BOLD));
			layout = new FormData();
			layout.top=new FormAttachment(tblextendedtrialbal,20);
			layout.left=new FormAttachment(25);
			btnViewTbForAccount.setLayoutData(layout);

			
		    btnPrint =new Button(this,SWT.PUSH);
			btnPrint.setText(" &Print ");
			btnPrint.setFont(new Font(display,"Times New Roman",10,SWT.BOLD));
			layout = new FormData();
			layout.top=new FormAttachment(tblextendedtrialbal,20);
			layout.left=new FormAttachment(60);
			btnPrint.setLayoutData(layout);

		
		
		
	
	this.makeaccessible(tblextendedtrialbal);
	this.getAccessible();
	//this.setEvents();
	//this.pack();
	this.setBounds(this.getDisplay().getPrimaryMonitor().getBounds());
	try {
			sheetStream = ODPackage.createFromStream(this.getClass().getResourceAsStream("/templates/ExtendedTrialBal.ots"),"ExtendedTrialBal");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	this.setReport(tbData2);
	setEvents();
	
}
	
	private void setReport(Object[]tbData2)
	{
		String[] columns;
		columns=new String[]{"Sr.No","Account Name","Group Name","Opening Balance","Total Debit Transaction","Total Credit Transaction","Debit Balance","Credit Balance"};
		
		lblsrno = new Label(tblextendedtrialbal,SWT.BORDER|SWT.LEFT );
		lblsrno.setFont(new Font(display, "Times New Roman",10,SWT.BOLD));
		lblsrno.setText("Sr No.");
		
	
		lblaccname = new Label(tblextendedtrialbal,SWT.BORDER|SWT.CENTER);
		lblaccname.setFont(new Font(display, "Times New Roman",10,SWT.BOLD));
		lblaccname.setText("Account Name");
		
		lblgrpname = new Label(tblextendedtrialbal,SWT.BORDER|SWT.CENTER);
		lblgrpname.setFont(new Font(display, "Times New Roman",10,SWT.BOLD));
		lblgrpname.setText("Group Name");
		
		lblopeningbal = new Label(tblextendedtrialbal,SWT.BORDER|SWT.CENTER);
		lblopeningbal.setFont(new Font(display, "Times New Roman",10,SWT.BOLD));
		lblopeningbal.setText("Opening\nBalance");
		
		lbltotaldrtransac = new Label(tblextendedtrialbal,SWT.BORDER|SWT.CENTER);
		lbltotaldrtransac.setFont(new Font(display, "Times New Roman",10,SWT.BOLD));
		lbltotaldrtransac.setText("Total Debit\nTransactions");
		
		lbltotalcrtransac = new Label(tblextendedtrialbal,SWT.BORDER|SWT.CENTER);
		lbltotalcrtransac.setFont(new Font(display, "Times New Roman",10,SWT.BOLD));
		lbltotalcrtransac.setText("Total Credit\nTransactions");
		
		lbldrbal = new Label(tblextendedtrialbal,SWT.BORDER|SWT.CENTER);
		lbldrbal.setFont(new Font(display, "Times New Roman",10,SWT.BOLD));
		lbldrbal.setText("Debit\n Balance");
		
		lblcrbal = new Label(tblextendedtrialbal,SWT.BORDER|SWT.CENTER);
		lblcrbal.setFont(new Font(display, "Times New Roman",10,SWT.BOLD));
		lblcrbal.setText("Credit\n Balance");
		
		
			
		headerRow = new TableItem(tblextendedtrialbal, SWT.BORDER);
		
		TableItem[] items = tblextendedtrialbal.getItems();
		
		srno = new TableColumn(tblextendedtrialbal ,SWT.BORDER |SWT.BACKGROUND| SWT.CENTER );
		accname = new TableColumn(tblextendedtrialbal, SWT.LEFT);
		grpname = new TableColumn(tblextendedtrialbal, SWT.CENTER);
		openingbal = new TableColumn(tblextendedtrialbal, SWT.RIGHT);
		totaldrtransac = new TableColumn(tblextendedtrialbal, SWT.RIGHT);
		totalcrtransac = new TableColumn(tblextendedtrialbal, SWT.RIGHT);
		drbal = new TableColumn(tblextendedtrialbal, SWT.RIGHT);
		crbal = new TableColumn(tblextendedtrialbal, SWT.RIGHT);
	
		
		TableEditor editorsrno = new TableEditor(tblextendedtrialbal);
		editorsrno.grabHorizontal = true;
		editorsrno.setEditor(lblsrno,items[0],0);
				
		TableEditor editorAccName = new TableEditor(tblextendedtrialbal);
		editorAccName.grabHorizontal = true;
		editorAccName.setEditor(lblaccname,items[0],1);
		
		TableEditor editorgrpName = new TableEditor(tblextendedtrialbal);
		editorgrpName.grabHorizontal = true;
		editorgrpName.setEditor(lblgrpname,items[0],2);
		
		TableEditor editoropeningbal = new TableEditor(tblextendedtrialbal);
		editoropeningbal.grabHorizontal = true;
		editoropeningbal.setEditor(lblopeningbal,items[0],3);
		
		TableEditor editorDrtransac = new TableEditor(tblextendedtrialbal);
		editorDrtransac.grabHorizontal = true;
		editorDrtransac.setEditor(lbltotaldrtransac,items[0],4);
		
		TableEditor editorCrtransac = new TableEditor(tblextendedtrialbal);
		editorCrtransac.grabHorizontal = true;
		editorCrtransac.setEditor(lbltotalcrtransac,items[0],5);
		
		TableEditor editordrbal = new TableEditor(tblextendedtrialbal);
		editordrbal.grabHorizontal = true;
		editordrbal.setEditor(lbldrbal,items[0],6);
		
		TableEditor editorcrbal = new TableEditor(tblextendedtrialbal);
		editorcrbal.grabHorizontal = true;
		editorcrbal.setEditor(lblcrbal,items[0],7);
		
		final int tblwidth = tblextendedtrialbal.getClientArea().width;
		tblextendedtrialbal.addListener(SWT.MeasureItem, new Listener() 
		{
			@Override
			public void handleEvent(Event event) {
				// TODO Auto-generated method stub
				//event.width = 150;
				srno.setWidth(4 * tblwidth / 100);
				accname.setWidth(30  * tblwidth / 100);
				grpname.setWidth(15 * tblwidth / 100);
				openingbal.setWidth(9 * tblwidth / 100);
				totaldrtransac.setWidth(9 * tblwidth / 100);
				totalcrtransac.setWidth(9 * tblwidth / 100);
				drbal.setWidth(9 * tblwidth / 100);
				crbal.setWidth(9 * tblwidth / 100);
				event.height = 32;
			}
		});
		srno.pack();
		accname.pack();
		grpname.pack();
		openingbal.pack();
		totaldrtransac.pack();
		totalcrtransac.pack();
		drbal.pack();
		crbal.pack();	
		
		double drbal = 0.00;
		double crbal = 0.00;
		btnPrint.setData("printcolumns",columns);
		for(int tbcounter = 0; tbcounter< tbData2.length; tbcounter ++)
		{
			TableItem tbrow = new TableItem(tblextendedtrialbal , SWT.NONE);
			tbrow.setFont(new Font(display, "Times New Roman", 10, SWT.NORMAL));
			Object[] tbrecord = (Object[]) tbData2[tbcounter];
			Object[] extendedRecord = (Object[]) tbData2[tbcounter];

			if(tbcounter < tbData2.length-1)
			{
				tbrow.setText(0, tbrecord[0].toString());
				Button btnAccount = new Button(tblextendedtrialbal , SWT.FLAT);
				btnAccount.setFont( new Font(display,"Times New Roman", 10, SWT.NORMAL | SWT.BOLD) );
				TableEditor btnedit = new TableEditor(tblextendedtrialbal);
				btnAccount.setText(tbrecord[1].toString());
				btnedit.grabHorizontal = true;
				btnedit.setEditor(btnAccount,tbrow,1 );
				accounts.add(btnAccount);
				tbrow.setText(2, tbrecord[2].toString());
				tbrow.setText(3, tbrecord[3].toString());
				tbrow.setText(4, tbrecord[4].toString());
				tbrow.setText(5, tbrecord[5].toString());
				tbrow.setText(6, tbrecord[6].toString());
				tbrow.setText(7, tbrecord[7].toString());
				
				Object[] printableRow = new Object[]{extendedRecord[0], extendedRecord[1],extendedRecord[2],extendedRecord[3], extendedRecord[4], extendedRecord[5], extendedRecord[6], extendedRecord[7]};
				printExtendedTrial.add(printableRow);
			}
			else
			{
				tbrow.setText(0,"");
				tbrow.setText(1,"");
				tbrow.setText(2,"");
				tbrow.setText(3,"Total");
				tbrow.setFont(new Font(display, "Times New Roman",10,SWT.BOLD|SWT.CENTER ));
				tbrow.setText(4,tbrecord[2].toString());
				tbrow.setText(5,tbrecord[3].toString());
				tbrow.setText(6, tbrecord[0].toString());
				tbrow.setText(7, tbrecord[1].toString());

				Object[] printableRow = new Object[]{"","","","Total", extendedRecord[2],extendedRecord[3],extendedRecord[0],extendedRecord[1]};
				printExtendedTrial.add(printableRow);
			}
			
			
		}
		TableItem closingRow = new TableItem(tblextendedtrialbal , SWT.NONE);
		closingRow.setFont(new Font(display, "Times New Roman", 10, SWT.NORMAL));
		//now get the Total Dr and Total Cr.
		//both can be got from the last row of tbdata.
		//get the last row (tbdata.len -1)
		//then access row[0] for TotalDr and row[1] for totalCr.
		//if dr is greater than cr then it is a dr balance.
		//substract the cr amount from dr to get the diff.
		//do exactly the other way round for cr > dr.
		nf = NumberFormat.getInstance();
		nf.setGroupingUsed(false);
		nf.setMaximumFractionDigits(2);
		nf.setMinimumFractionDigits(2);
		
		Object[] lastRow = (Object[]) tbData2[tbData2.length-1 ];
		drbal= Double.parseDouble(lastRow[0].toString());
		crbal= Double.parseDouble(lastRow[1].toString());
		closingRow.setFont(new Font(display, "Times New Roman",10,SWT.BOLD|SWT.CENTER));
		Double diffbal = 0.00;
		if(drbal > crbal)
		{
			diffbal = drbal - crbal;
			closingRow.setText(1,"Difference In Trial Balance");
			closingRow.setText(7,nf.format(diffbal));
			
			Object[] printableRow = new Object[]{""," Difference in Trial Balance","","","","","", diffbal};
			printExtendedTrial.add(printableRow);
		}
		if(crbal > drbal)
		{
			diffbal = crbal - drbal;
			closingRow.setText(1,"Difference in Trial Balance");
			closingRow.setText(6,nf.format(diffbal));
			
			Object[] printableRow = new Object[]{""," Difference in Trial Balance","","","","", diffbal,""};
			printExtendedTrial.add(printableRow);
		}
		TableItem totaldrcr = new TableItem(tblextendedtrialbal , SWT.NONE|SWT.SEPARATOR|SWT.BOLD);
		
		totaldrcr.setFont(new Font(display, "Times New Roman",10,SWT.BOLD|SWT.CENTER));
		if(drbal>crbal)
		{
			totaldrcr.setText(6, lastRow[0].toString());
			totaldrcr.setText(7, lastRow[0].toString());
			
			Object[] printableRow = new Object[]{"","","","","","",lastRow[0],lastRow[0]};
			printExtendedTrial.add(printableRow);
		}
		if(crbal>drbal)
		{
			totaldrcr.setText(6, lastRow[1].toString());
			totaldrcr.setText(7, lastRow[1].toString());
			
			Object[] printableRow = new Object[]{"","","","","","",lastRow[1],lastRow[1]};
			printExtendedTrial.add(printableRow);
		}
		tblextendedtrialbal.pack();
	}
private void setEvents()
{	
	btnViewTbForAccount.addSelectionListener(new SelectionAdapter() {
		@Override
		public void widgetSelected(SelectionEvent arg0) {
			// TODO Auto-generated method stub
			//super.widgetSelected(arg0);
			
			
			Composite grandParent = (Composite) btnViewTbForAccount.getParent().getParent();
			btnViewTbForAccount.getParent().dispose();
				
				viewTrialBalance vl=new viewTrialBalance(grandParent,SWT.NONE);
				vl.setSize(grandParent.getClientArea().width,grandParent.getClientArea().height);
			}
	
	});

	
		tblextendedtrialbal.setFocus();
		tblextendedtrialbal.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusGained(arg0);
				if(accounts.isEmpty())
				{
					btnViewTbForAccount.setFocus();
				}
				else
				{
				accounts.get(0).setFocus();
				}
			
		}
	} );
		
	btnViewTbForAccount.addKeyListener(new KeyAdapter() {
		@Override
		public void keyPressed(KeyEvent arg0) {
			// TODO Auto-generated method stub
			//super.keyPressed(arg0);
			if(arg0.keyCode==SWT.ARROW_RIGHT)
			{
				btnPrint.setFocus();
			}
		}
	});
	
	btnPrint.addKeyListener(new KeyAdapter() {
		@Override
		public void keyPressed(KeyEvent arg0) {
			// TODO Auto-generated method stub
			//super.keyPressed(arg0);
			if(arg0.keyCode==SWT.ARROW_LEFT)
			{
				btnViewTbForAccount.setFocus();
			}
		}
	});
		
	for(int accountcounter = 0; accountcounter < accounts.size(); accountcounter ++ )
	{
		accounts.get(accountcounter).addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				Button btncurrent = (Button) arg0.widget;
				String fromdate=globals.session[2].toString().substring(6)+"-"+globals.session[2].toString().substring(3,5)+"-"+globals.session[2].toString().substring(0,2);
				String todate=globals.session[3].toString().substring(6)+"-"+globals.session[3].toString().substring(3,5)+"-"+globals.session[3].toString().substring(0,2);

				Composite grandParent = (Composite) btncurrent.getParent().getParent().getParent();
				
				reportController.showLedger(grandParent, btncurrent.getText(), fromdate, todate, "No Project", true, true, false, "Extended Trial Balance", "");		btncurrent.getParent().getParent().dispose();
				
			}
		});
		accounts.get(accountcounter).addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.ARROW_DOWN && counter < accounts.size()-1 )
				{
					counter++;
					if(counter >= 0 && counter < accounts.size())
					{
						
					accounts.get(counter).setFocus();
					}
				}
				if(arg0.keyCode==SWT.ARROW_UP && counter > 0)
				{
					counter--;
					if(counter >= 0&& counter < accounts.size())
					{
						
						accounts.get(counter).setFocus();
					}
				}
			
				
			}

		});
	}
	
	btnPrint.addSelectionListener(new SelectionAdapter() {
		@Override
		public void widgetSelected(SelectionEvent arg0) {
			// TODO Auto-generated method stub
			//super.widgetSelected(arg0);
			String[] columns = (String[]) btnPrint.getData("printcolumns");
			Object[][] finalData =new Object[printExtendedTrial.size()][columns.length ];
			for(int counter = 0; counter < printExtendedTrial.size(); counter ++ )
			{
				Object[] printRow = (Object[]) printExtendedTrial.get(counter);												
				finalData[counter] = printRow;
			}
				
			//printLedgerData.copyInto(finalData);
			
				TableModel model = new DefaultTableModel(finalData,columns);
				try {
					final File extended = new File("/tmp/gnukhata/Report_Output/ExtendedTrialBalance" );
					/*SpreadSheet.createEmpty(model).saveAs(extended);*/
					final Sheet extendedSheet = sheetStream.getSpreadSheet().getFirstSheet();
					extendedSheet.ensureRowCount(100000);
					/*extendedSheet.getColumn(0).setWidth(new Integer(30));
					extendedSheet.getColumn(1).setWidth(new Integer(50));
					extendedSheet.getColumn(2).setWidth(new Integer(50));
					extendedSheet.getColumn(3).setWidth(new Integer(35));
					extendedSheet.getColumn(4).setWidth(new Integer(45));
					extendedSheet.getColumn(5).setWidth(new Integer(45));
					extendedSheet.getColumn(6).setWidth(new Integer(40));
					extendedSheet.getColumn(7).setWidth(new Integer(40));
					*/
					extendedSheet.getCellAt(0,0).setValue(globals.session[1].toString());
					extendedSheet.getCellAt(0,1).setValue("Extended Trial Balance For The Period From "+globals.session[2]+" To "+ globals.session[3]);
					for(int rowcounter=0; rowcounter<printExtendedTrial.size(); rowcounter++)
					{
						Object[] printrow = (Object[]) printExtendedTrial.get(rowcounter);
						//OOUtils.open(extendedSheet.getSpreadSheet().saveAs(extended));
						extendedSheet.getCellAt(0, rowcounter+3).setValue(printrow[0].toString());
						extendedSheet.getCellAt(1, rowcounter+3).setValue(printrow[1].toString());
						extendedSheet.getCellAt(2, rowcounter+3).setValue(printrow[2].toString());
						extendedSheet.getCellAt(3, rowcounter+3).setValue(printrow[3].toString());
						extendedSheet.getCellAt(4, rowcounter+3).setValue(printrow[4].toString());
						extendedSheet.getCellAt(5, rowcounter+3).setValue(printrow[5].toString());
						extendedSheet.getCellAt(6, rowcounter+3).setValue(printrow[6].toString());
						extendedSheet.getCellAt(7, rowcounter+3).setValue(printrow[7].toString());
						//				OOUtils.open(extended);
					}
					OOUtils.open(extendedSheet.getSpreadSheet().saveAs(extended));
				} 
					catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
	});
}
	
	public void makeaccessible(Control c)
	{
	/*
	 * getAccessible() method is the method of class Controlwhich is the
	 * parent class of all the UI components of SWT including Shell.so when
	 * the shell is made accessible all the controls which are contained by
	 * that shell are made accessible automatically.
	 */
		c.getAccessible();
	}



	protected void checkSubclass()
	{
	//this is blank method so will disable the check that prevents subclassing of shells.
	}
	/*public static void main(String[] args)
	{
		Display d = new Display();
		Shell s= new Shell(d);
		int vouchercode = 0;
		String voucherType = null;
		Object[] tbData2 = null;
		viewextendedtrialbalreport vetbr=new viewextendedtrialbalreport(s, SWT.NONE, tbData2);
		vetbr.setSize(s.getClientArea().width, s.getClientArea().height );
		
		//s.setSize(400, 400);
		s.pack();
		s.open();
		while (!s.isDisposed() ) {
			if (!d.readAndDispatch())
			{
				 d.sleep();
				 if(! s.getMaximized())
				 {
					 s.setMaximized(true);
				 }
			}
		}
		
	}*/
}
